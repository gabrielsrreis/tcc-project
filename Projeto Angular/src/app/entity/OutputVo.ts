import { SolucaoVO } from "./SolucaoVo";

export class OutputVO{
    out : Array<SolucaoVO>
    executionTime : number
    numSolucoes : number
    algoritmo: string
    solucao = new SolucaoVO()
}