export class ConfigurationVo{
    populationSize = 182;
	maxEvaluations = 100000;
	feedBack = 20;
	probabilityMut = 0.8;
	crossover = 'SinglePointCrossover';
	distributionIndex = 20;
	mutation = 'BitFlipMutation';
	selection = 'BinaryTournament';
}