import { OutputVO } from './OutputVo';
import { FiltroVo } from './FiltroVo';
import { ConfigurationVo } from './ConfigurationVo';

export class ProjetoVo{
    resultados = new Array<OutputVO>();
    filtro: FiltroVo;
    conf: ConfigurationVo

}