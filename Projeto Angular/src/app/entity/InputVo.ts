import { ConfigurationVo } from './ConfigurationVo';
import { FiltroVo } from './FiltroVo';

export class InputVO{
    cost : string
    dependencia : string
    prioridade : string
    quantTarefas : number
    objetivos : number
    algoritm : string
    configuration = new ConfigurationVo()
    filtro = new FiltroVo()
}