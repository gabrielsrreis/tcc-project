
export class FiltroVo{
    maxCost: number 
    minCost: number 
    maxVal: number 
    minVal: number
    minRisk: number
    maxRisk: number
}