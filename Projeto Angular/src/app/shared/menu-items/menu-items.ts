import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}

const MENUITEMS = [
  { state: 'starter', name: 'Sobre', type: 'link', icon: 'home' },
  { state: 'form', type: 'link', name: 'Pesquisar', icon: 'search' },
  { state: 'results', type: 'link', name: 'Resultados', icon: 'description' }
  // { state: 'chips', type: 'link', name: 'Configurações', icon: 'settings' }
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
