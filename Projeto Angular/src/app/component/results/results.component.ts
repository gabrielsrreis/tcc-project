import { Component } from '@angular/core';
import { ProjetoVo } from '../../entity/ProjetoVo';
import { SolucaoVO } from '../../entity/SolucaoVo';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { FormService } from '../form/form.service';
import { OutputVO } from '../../entity/OutputVo';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent {
  public scatterChartData: any = {};
  public tableChartData: any = {};
  output = new OutputVO();
  outputComp = new OutputVO();
  projeto: ProjetoVo;
  filtros: Array<SolucaoVO>
  maxCost: number
  minCost: number
  maxVal: number
  minVal: number
  minRisk: number
  maxRisk: number
  render = false
  dataTable;

  view: any[] = [1000, 400];

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = 'Custo';
  showYAxisLabel = true;
  yAxisLabel = 'Valor';

  selected = [];

  rows;
  columns = [
    { custo: 'Custo' },
    { valor: 'Valor' },
    { risco: 'Risco' }
  ];

  multi = [
    {
      name: 'Soluçoes',
      series: [
      ]
    },
  ];

  colorScheme = {
    domain: ['#1e88e5']
  };

  constructor(private route: ActivatedRoute, private router: Router, private service: FormService) { }

  ngOnInit() {
    let resposta
    this.route.queryParams.subscribe(params => {
      resposta = params["projeto"];
    });
    if (resposta === undefined || resposta === []) {
      this.novo();
      return;
    } else {
      this.projeto = JSON.parse(resposta);
      this.output = this.projeto.resultados.pop();
      this.output.solucao = new SolucaoVO()
      if (this.projeto.resultados.length === 1) {
        this.outputComp = this.projeto.resultados.pop()
        this.render = true
      }
    }
    this.filtros = this.output.out
    if(this.filtros[0].objetivos[0]===this.filtros[1].objetivos[0] && this.filtros[1].objetivos[0]===this.filtros[2].objetivos[0]){
      this.filtros = []
      this.output.out = []
      alert("Não há resultados para essa busca")
    }
    this.carregarGrafico();
  }

  carregarGrafico() {
    var listTable = [];
    let series = []
    listTable = []
    for (let index = 0; index < this.filtros.length; index++) {
      const element = this.filtros[index];
      if (element.objetivos.length === 2) {
        listTable = listTable.concat({ custo: element.objetivos[0], valor: element.objetivos[1], risco: 0 });
        series = series.concat({ name: element.conjunto, x: element.objetivos[0], y: element.objetivos[1], r: 0 });
      } else {
        listTable = listTable.concat({ custo: element.objetivos[0], valor: element.objetivos[1], risco: element.objetivos[2] });
        series = series.concat({ name: element.conjunto, x: element.objetivos[0], y: element.objetivos[1], r: element.objetivos[2] })
      }
    }
    this.multi = [
      {
        name: 'Solução',
        series: series
      },
    ];
    this.rows = listTable;
    this.dataTable = series
  }

  filtrar() {
    this.filtros = new Array<SolucaoVO>()
    
    for (let index = 0; index < this.output.out.length; index++) {
      let item = this.output.out[index];
      let add = true
      if (item.objetivos[0] > this.maxCost && this.maxCost !== null) {
        add = false
      }
      if (item.objetivos[0] < this.minCost && this.minCost !== null) {
        add = false
      }
      if (item.objetivos[1] > this.maxVal && this.maxVal !== null) {
        add = false
      }

      if (item.objetivos[1] < this.minVal && this.minVal !== null) {
        add = false
      }
      if (item.objetivos.length === 3) {
        if (item.objetivos[2] > this.maxRisk && this.maxRisk !== null) {
          add = false
        }
        if (item.objetivos[2] < this.minRisk && this.minRisk !== null) {
          add = false
        }
      }
      if (add) {
        this.filtros.push(item);
      }
    }
    if (this.filtros.length === 0) {
      alert("Não existe soluções encontradas")
      this.filtros = this.output.out
    }
    this.carregarGrafico();
  }

  selectable({ selected }) {
    let custo = selected[0].custo
    let valor = selected[0].valor
    let risk = undefined
    this.output.solucao = this.buscarsolucao(custo, valor, risk);
  }

  onSelect(event) {
    let custo = event.name
    let valor = event.value
    let risk = undefined
    this.output.solucao = this.buscarsolucao(custo, valor, risk);
  }

  public isComp() {
    return this.outputComp !== undefined
  }

  buscarsolucao(custo: number, valor: number, risk: number) {
    for (let index = 0; index < this.filtros.length; index++) {
      let element = this.filtros[index];
      let objtValor = element.objetivos[1]
      if (element.objetivos[0] === custo && objtValor === valor) {
        if (risk !== undefined && element.objetivos[2] === risk) {
          return element;
        } else {
          return element;
        }
      }
    }
  }

  public mouseOver(event) {
    console.log("Tec")
  }

  public comparar() {
    if (this.output.solucao === undefined || this.output.solucao.conjunto === undefined) {
      alert("Selecione uma solução para comparar.")
      return;
    }
    this.projeto.resultados.push(this.output);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "projeto": JSON.stringify(this.projeto)
      }
    };

    this.router.navigate(['/form'], navigationExtras);
  }

  public novo() {
    if (this.projeto === undefined) {
      this.router.navigate(['/form'] );
      alert("Não há resultados para se analisar")
    }
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "projeto": JSON.stringify(this.projeto)
      }
    };

    this.router.navigate(['/form'], navigationExtras);
  }
}
