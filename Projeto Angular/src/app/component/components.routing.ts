import { Routes } from '@angular/router';
import { FormComponent } from './form/form.component';
import { ResultsComponent } from './results/results.component';

export const ComponentsRoutes: Routes = [
  {
    path: 'form',
    component: FormComponent
  },
  {
    path: 'results',
    component: ResultsComponent
  }
];
