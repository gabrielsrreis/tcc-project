import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InputVO } from '../../entity/InputVo';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  urlPost="http://localhost:8080/RestNRP/services/nrp/processPost"
  urlGet="http://localhost:8080/RestNRP/services/nrp/testeGet"

  constructor(private http: HttpClient){

  }

  headers = new Headers({'Access-Control-Allow-Headers': 'Content-Type', 'Content-Type': 'application/json'});

  testarPost(input : InputVO) {
    return this.http.post(this.urlPost, input)
  }
}
