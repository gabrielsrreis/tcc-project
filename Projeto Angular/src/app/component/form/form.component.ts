import { Component, Input } from '@angular/core';
import { OutputVO } from '../../entity/OutputVo';
import { NavigationExtras, Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ProjetoVo } from '../../entity/ProjetoVo';
import { InputVO } from '../../entity/InputVo';
import { FormService } from './form.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent {
  input = new InputVO();
  output = new OutputVO();
  tabela = false
  projeto: ProjetoVo;

  rows1 = [];
  rows2;
  rows3;

  public isSpinnerVisible = false;

  @Input()
  public backgroundColor = 'rgba(0, 115, 170, 0.69)';

  constructor(private route: ActivatedRoute, private router: Router, private service: FormService) { }

  ngOnInit() {
    let resposta
    this.route.queryParams.subscribe(params => {
      resposta = params["projeto"];
    });
    if (resposta !== undefined && resposta !== []) {
      this.projeto = JSON.parse(resposta);
      this.input.filtro = this.projeto.filtro
      this.input.configuration = this.projeto.conf
    } else if (this.projeto === undefined) {
      this.projeto = new ProjetoVo();
    }
    this.input.algoritm = 'N'
    setTimeout(() => {
    }, 3000);
  }
  files: File;
  filestring: String;

  handleReaderLoadedDependencia(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.input.dependencia = binaryString
    this.rows2 = this.criarList(this.input.dependencia);
  }

  handleReaderLoadedPrioridade(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.input.prioridade = binaryString
    this.rows3 = this.criarList(this.input.prioridade);
  }

  handleReaderLoadedCost(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.input.cost = binaryString
    this.rows1 = this.criarList(this.input.cost);
  }

  public criarList(dados: string) {
    var list = []
    let lines = dados.split("\n");
    lines.forEach(element => {
      let line = element.split(";")
      list.push(line)
    });
    return list;
  }

  onChangeDependencia(event) {
    var file = event.target.files[0];
    var reader = new FileReader();
    reader.onload = this.handleReaderLoadedDependencia.bind(this);
    reader.readAsBinaryString(file);
  }

  onChangeCost(event) {
    var file = event.target.files[0];
    var reader = new FileReader();
    reader.onload = this.handleReaderLoadedCost.bind(this);
    reader.readAsBinaryString(file);
  }
  onChangePrioridade(event) {
    var file = event.target.files[0];
    var reader = new FileReader();
    reader.onload = this.handleReaderLoadedPrioridade.bind(this);
    reader.readAsBinaryString(file);
  }

  processar(form: NgForm) {
    if (this.input.algoritm === undefined) {
      alert('É nescessario informar o Algoritmo de busca.');
      return;
    }

    if (this.input.cost === undefined) {
      alert('É nescessario informar os valores');
      return;
    }
    this.isSpinnerVisible = true;
    this.projeto.filtro = this.input.filtro
    this.projeto.conf = this.input.configuration
    this.service.testarPost(this.input).subscribe(
      (resposta: OutputVO) => {
        this.projeto.resultados.push(resposta);
        let navigationExtras: NavigationExtras = {
          queryParams: {
            "projeto": JSON.stringify(this.projeto)
          }
        };

        this.router.navigate(['/results'], navigationExtras);
        this.isSpinnerVisible = false;
      },
      (error: any) => {
        alert(error);
        this.isSpinnerVisible = false;
      }
    );
  }
}
