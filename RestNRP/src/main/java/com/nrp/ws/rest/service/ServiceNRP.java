package com.nrp.ws.rest.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.nrp.ws.rest.vo.InputVO;

import regras.Process;

@Path("/nrp")
public class ServiceNRP {

	@POST
	@Path("/processPost")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response teste(InputVO input) {
		ResponseBuilder builder = Response.ok(new Process().processar(input));
		return builder.build();
	}

	@GET
	@Path("testeGet")
	@Produces(MediaType.APPLICATION_JSON)
	public Response testeGet() {
		InputVO input = new InputVO();
		ResponseBuilder builder = Response.ok(input);
		return builder.build();
	}

}
