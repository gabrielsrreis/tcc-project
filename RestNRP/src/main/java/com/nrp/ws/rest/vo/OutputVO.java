package com.nrp.ws.rest.vo;

import java.util.List;

public class OutputVO {
	List<SolucaoVO> out;
	private Long executionTime;
	private Integer numSolucoes;
	private String algoritmo;

	public List<SolucaoVO> getOut() {
		return out;
	}

	public void setOut(List<SolucaoVO> out) {
		this.out = out;
	}

	public Long getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(Long executionTime) {
		this.executionTime = executionTime;
	}

	public Integer getNumSolucoes() {
		return numSolucoes;
	}

	public void setNumSolucoes(Integer numSolucoes) {
		this.numSolucoes = numSolucoes;
	}

	public String getAlgoritmo() {
		return algoritmo;
	}

	public void setAlgoritmo(String algoritmo) {
		this.algoritmo = algoritmo;
	}
}
