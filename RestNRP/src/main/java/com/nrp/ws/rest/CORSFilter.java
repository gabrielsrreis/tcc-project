package com.nrp.ws.rest;

import javax.ws.rs.ext.Provider;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;

@Provider
public class CORSFilter implements ContainerResponseFilter {

    public ContainerResponse filter(ContainerRequest request, ContainerResponse response) {
        response.getHttpHeaders().add("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
//        response.getHttpHeaders().add("Access-Control-Max-Age", "3600");
        response.getHttpHeaders().add("Access-Control-Allow-Headers", "x-requested-with, content-type, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Access-Control-Allow-Methods");
        response.getHttpHeaders().add("Access-Control-Allow-Origin", "http://localhost:4200");
		return response;
	}
    
}