package com.nrp.ws.rest.vo;

import java.io.Serializable;

public class Configuration implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1759465190377488559L;
	private Integer populationSize;
	private Integer maxEvaluations;
	private Integer feedBack;
	private Double probabilityMut;
	private String crossover;
	private Integer distributionIndex;
	private String mutation;
	private String selection;
	public Integer getPopulationSize() {
		return populationSize;
	}
	public void setPopulationSize(Integer populationSize) {
		this.populationSize = populationSize;
	}
	public Integer getMaxEvaluations() {
		return maxEvaluations;
	}
	public void setMaxEvaluations(Integer maxEvaluations) {
		this.maxEvaluations = maxEvaluations;
	}
	public Integer getFeedBack() {
		return feedBack;
	}
	public void setFeedBack(Integer feedBack) {
		this.feedBack = feedBack;
	}
	public Double getProbabilityMut() {
		return probabilityMut;
	}
	public void setProbabilityMut(Double probabilityMut) {
		this.probabilityMut = probabilityMut;
	}
	public String getCrossover() {
		return crossover;
	}
	public void setCrossover(String crossover) {
		this.crossover = crossover;
	}
	public Integer getDistributionIndex() {
		return distributionIndex;
	}
	public void setDistributionIndex(Integer distributionIndex) {
		this.distributionIndex = distributionIndex;
	}
	public String getMutation() {
		return mutation;
	}
	public void setMutation(String mutation) {
		this.mutation = mutation;
	}
	public String getSelection() {
		return selection;
	}
	public void setSelection(String selection) {
		this.selection = selection;
	}
	
	
}
