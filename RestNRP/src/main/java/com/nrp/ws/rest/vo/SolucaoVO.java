package com.nrp.ws.rest.vo;

import java.util.List;

public class SolucaoVO {
	List<Double> objetivos;
	String conjunto;
	public String getConjunto() {
		return conjunto;
	}
	public void setConjunto(String conjunto) {
		this.conjunto = conjunto;
	}
	public List<Double> getObjetivos() {
		return objetivos;
	}
	public void setObjetivos(List<Double> objetivos) {
		this.objetivos = objetivos;
	}
}
