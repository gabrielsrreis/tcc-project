package com.nrp.ws.rest.vo;

import java.io.Serializable;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class InputVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7717877254650876962L;
	private Integer quantTarefas;
	private Integer objetivos;
	private String cost;
    private String prioridade;
    private String dependencia;
    private Configuration configuration;
    private Filtro filtro;
    private String algoritm;
	public Integer getQuantTarefas() {
		return quantTarefas;
	}
	public void setQuantTarefas(Integer quantTarefas) {
		this.quantTarefas = quantTarefas;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getPrioridade() {
		return prioridade;
	}
	public void setPrioridade(String prioridade) {
		this.prioridade = prioridade;
	}
	public String getDependencia() {
		return dependencia;
	}
	public void setDependencia(String dependencia) {
		this.dependencia = dependencia;
	}
	public Integer getObjetivos() {
		return objetivos;
	}
	public void setObjetivos(Integer objetivos) {
		this.objetivos = objetivos;
	}
	public Configuration getConfiguration() {
		return configuration;
	}
	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}
	public Filtro getFiltro() {
		return filtro;
	}
	public void setFiltro(Filtro filtro) {
		this.filtro = filtro; 
	}
	public String getAlgoritm() {
		return algoritm;
	}
	public void setAlgoritm(String algoritm) {
		this.algoritm = algoritm;
	}
}
