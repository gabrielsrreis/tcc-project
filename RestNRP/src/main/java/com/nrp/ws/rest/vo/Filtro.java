package com.nrp.ws.rest.vo;

import java.io.Serializable;

public class Filtro implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4987067406196150089L;
	private Integer maxCost; 
	private Integer minCost; 
	private Integer maxVal; 
	private Integer minVal;
	private Integer minRisk;
	private Integer maxRisk;
	public Integer getMaxCost() {
		return maxCost;
	}
	public void setMaxCost(Integer maxCost) {
		this.maxCost = maxCost;
	}
	public Integer getMinCost() {
		return minCost;
	}
	public void setMinCost(Integer minCost) {
		this.minCost = minCost;
	}
	public Integer getMaxVal() {
		return maxVal;
	}
	public void setMaxVal(Integer maxVal) {
		this.maxVal = maxVal;
	}
	public Integer getMinVal() {
		return minVal;
	}
	public void setMinVal(Integer minVal) {
		this.minVal = minVal;
	}
	public Integer getMinRisk() {
		return minRisk;
	}
	public void setMinRisk(Integer minRisk) {
		this.minRisk = minRisk;
	}
	public Integer getMaxRisk() {
		return maxRisk;
	}
	public void setMaxRisk(Integer maxRisk) {
		this.maxRisk = maxRisk;
	}
	
}
