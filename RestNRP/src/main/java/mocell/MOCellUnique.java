package mocell;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.encodings.variable.Binary;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.Distance;
import jmetal.util.JMException;
import jmetal.util.Neighborhood;
import jmetal.util.Ranking;
import jmetal.util.archive.CrowdingArchive;
import jmetal.util.comparators.CrowdingComparator;
import jmetal.util.comparators.DominanceComparator;

public class MOCellUnique  extends Algorithm{
	
	 public MOCellUnique(Problem problem){
		    super (problem) ;
		  }

		  /** Execute the algorithm 
		   * @throws JMException */
		  public SolutionSet execute() throws JMException, ClassNotFoundException {
		    //Init the parameters
		    int populationSize, archiveSize, maxEvaluations, evaluations;
		    Operator mutationOperator, crossoverOperator, selectionOperator;
		    SolutionSet currentPopulation;
		    CrowdingArchive archive;
		    SolutionSet [] neighbors;    
		    Neighborhood neighborhood;
		    Comparator dominance = new DominanceComparator();  
		    Comparator crowdingComparator = new CrowdingComparator();
		    Distance distance = new Distance();
		    
		    QualityIndicator indicators; // QualityIndicator object
			int requiredEvaluations; // Use in the example of use of the
			// indicators object (see below)
		    
		    // Read the parameters
		    populationSize    = ((Integer)getInputParameter("populationSize")).intValue();
		    archiveSize       = ((Integer)getInputParameter("archiveSize")).intValue();
		    maxEvaluations    = ((Integer)getInputParameter("maxEvaluations")).intValue(); 
		    indicators = (QualityIndicator) getInputParameter("indicators");

		    // Read the operators
		    mutationOperator  = operators_.get("mutation");
		    crossoverOperator = operators_.get("crossover");
		    selectionOperator = operators_.get("selection");        

		    // Initialize the variables    
		    currentPopulation  = new SolutionSet(populationSize);        
		    archive            = new CrowdingArchive(archiveSize,problem_.getNumberOfObjectives());                
		    evaluations        = 0;
		    requiredEvaluations = 0;
		    neighborhood       = new Neighborhood(populationSize-8);
		    neighbors          = new SolutionSet[populationSize];

		    // Create the initial population
		    for (int i = 0; i < populationSize; i++){
		      Solution individual = new Solution(problem_);
		      if (!contains(currentPopulation, individual)) {
			      problem_.evaluate(individual);           
			      problem_.evaluateConstraints(individual);
			      currentPopulation.add(individual);
			      individual.setLocation(i);
			      evaluations++;
		      }
		    }

		    // Main loop
		    while (evaluations < maxEvaluations){                                 
		      for (int ind = 0; ind < currentPopulation.size(); ind++){
		        Solution individual = new Solution(currentPopulation.get(ind));

		        Solution [] parents = new Solution[2];
		        Solution [] offSpring;

		        //neighbors[ind] = neighborhood.getFourNeighbors(currentPopulation,ind);
		        System.out.println("Log:");
		        System.out.println("ind:"+ind);
		        System.out.println("currentPopulation:"+currentPopulation.size());
		        neighbors[ind] = neighborhood.getEightNeighbors(currentPopulation,ind);                                                            
		        neighbors[ind].add(individual);

		        // parents
		        parents[0] = (Solution)selectionOperator.execute(neighbors[ind]);
		        if (archive.size() > 0) {
		          parents[1] = (Solution)selectionOperator.execute(archive);
		        } else {                   
		          parents[1] = (Solution)selectionOperator.execute(neighbors[ind]);
		        }

		        // Create a new individual, using genetic operators mutation and crossover
		        offSpring = (Solution [])crossoverOperator.execute(parents);               
		        mutationOperator.execute(offSpring[0]);
		        if (!contains(currentPopulation, offSpring[0])) {
		        // Evaluate individual an his constraints
		        problem_.evaluate(offSpring[0]);
		        problem_.evaluateConstraints(offSpring[0]);
		        evaluations++;

		        int flag = dominance.compare(individual,offSpring[0]);

		        if (flag == 1) { //The new individual dominates
		          offSpring[0].setLocation(individual.getLocation());                                      
		          currentPopulation.replace(offSpring[0].getLocation(),offSpring[0]);
		          archive.add(new Solution(offSpring[0]));                   
		        } else if (flag == 0) { //The new individual is non-dominated               
		          neighbors[ind].add(offSpring[0]);               
		          offSpring[0].setLocation(-1);
		          Ranking rank = new Ranking(neighbors[ind]);
		          for (int j = 0; j < rank.getNumberOfSubfronts(); j++) {
		            distance.crowdingDistanceAssignment(rank.getSubfront(j),
		                                                problem_.getNumberOfObjectives());
		          }
		          Solution worst = neighbors[ind].worst(crowdingComparator);

		          if (worst.getLocation() == -1) { //The worst is the offspring
		            archive.add(new Solution(offSpring[0]));
		          } else {
		            offSpring[0].setLocation(worst.getLocation());
		            currentPopulation.replace(offSpring[0].getLocation(),offSpring[0]);
		            archive.add(new Solution(offSpring[0]));
		          }
		        }
		       }
		     }
		      if ((indicators != null) && (requiredEvaluations == 0)) {
					double HV = indicators.getHypervolume(currentPopulation);
					if (HV >= (0.98 * indicators.getTrueParetoFrontHypervolume())) {
						requiredEvaluations = evaluations;
					} // if
				} // if
		    } 
		    
		 // Return as output parameter the required evaluations
			setOutputParameter("evaluations", requiredEvaluations);

		    archive.printFeasibleFUN("FUN_MOCell") ;

		    return archive;
    } // while
		  private boolean contains(SolutionSet population, Solution candidate) {
				Iterator<Solution> iterator = population.iterator();
				while (iterator.hasNext()) {
					Solution sol = iterator.next();
					if (equals(sol, candidate)) {
						return true;
					}
				}
				return false;
			}

			private boolean equals(Solution sol1, Solution sol2) {
				Binary s1 = (Binary)sol1.getDecisionVariables()[0];
				Binary s2 = (Binary)sol2.getDecisionVariables()[0];
				if (Arrays.equals(s1.bits_.toByteArray(), s2.bits_.toByteArray())){
					return true;
				}else{
					return false;
				}
			}	  
} // MOCell
