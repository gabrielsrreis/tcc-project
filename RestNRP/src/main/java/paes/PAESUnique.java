package paes;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.encodings.variable.Binary;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.JMException;
import jmetal.util.archive.AdaptiveGridArchive;
import jmetal.util.comparators.DominanceComparator;

public class PAESUnique extends Algorithm {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 188594633310360087L;

	/** 
	  * Create a new PAES instance for resolve a problem
	  * @param problem Problem to solve
	  */                 
	  public PAESUnique(Problem problem) {                
	    super (problem) ;
	  } // Paes
	    
	  /**
	   * Tests two solutions to determine which one becomes be the guide of PAES
	   * algorithm
	   * @param solution The actual guide of PAES
	   * @param mutatedSolution A candidate guide
	   */
	  public Solution test(Solution solution, 
	                       Solution mutatedSolution, 
	                       AdaptiveGridArchive archive){  
	    
	    int originalLocation = archive.getGrid().location(solution);
	    int mutatedLocation  = archive.getGrid().location(mutatedSolution); 

	    if (originalLocation == -1) {
	      return new Solution(mutatedSolution);
	    }
	    
	    if (mutatedLocation == -1) {
	      return new Solution(solution);
	    }
	        
	    if (archive.getGrid().getLocationDensity(mutatedLocation) < 
	        archive.getGrid().getLocationDensity(originalLocation)) {
	      return new Solution(mutatedSolution);
	    }
	    
	    return new Solution(solution);          
	  } // test
	    
	  /**   
	  * Runs of the Paes algorithm.
	  * @return a <code>SolutionSet</code> that is a set of non dominated solutions
	  * as a result of the algorithm execution  
	   * @throws JMException 
	  */    
	  public SolutionSet execute() throws JMException, ClassNotFoundException {     
	    int bisections, archiveSize, maxEvaluations, evaluations;
	    AdaptiveGridArchive archive;
	    Operator mutationOperator;
	    Comparator dominance;
	    
	    QualityIndicator indicators; // QualityIndicator object
		int requiredEvaluations; // Use in the example of use of the
		// indicators object (see below)
	    
	    //Read the params
	    bisections     = ((Integer)getInputParameter("biSections")).intValue();
	    archiveSize    = ((Integer)getInputParameter("archiveSize")).intValue();
	    maxEvaluations = ((Integer)getInputParameter("maxEvaluations")).intValue();
	    indicators = (QualityIndicator) getInputParameter("indicators");
	    
	    //Read the operators        
	    mutationOperator = this.operators_.get("mutation");        

	    //Initialize the variables                
	    evaluations = 0;
	    requiredEvaluations = 0;
	    archive     = new AdaptiveGridArchive(archiveSize,bisections,problem_.getNumberOfObjectives());        
	    dominance = new DominanceComparator();           
	            
	    //-> Create the initial solution and evaluate it and his constraints
	    Solution solution = new Solution(problem_);
	    if (!contains(archive, solution)) {
		    problem_.evaluate(solution);        
		    problem_.evaluateConstraints(solution);
		    evaluations++;
		        
		    // Add it to the archive
		    archive.add(new Solution(solution));            
	    }
	    //Iterations....
	    do {
	      // Create the mutate one
	      Solution mutatedIndividual = new Solution(solution);  
	      mutationOperator.execute(mutatedIndividual);
	      if (!contains(archive, mutatedIndividual)){  
	      problem_.evaluate(mutatedIndividual);                     
	      problem_.evaluateConstraints(mutatedIndividual);
	      evaluations++;
	      //<-
	      
	      // Check dominance
	      int flag = dominance.compare(solution,mutatedIndividual);            
	            
	      if (flag == 1) { //If mutate solution dominate                  
	        solution = new Solution(mutatedIndividual);                
	        archive.add(mutatedIndividual);                
	      } else if (flag == 0) { //If none dominate the other                               
	        if (archive.add(mutatedIndividual)) {                    
	          solution = test(solution,mutatedIndividual,archive);
	        }                                
	      }      
	      }
	      /*
	      if ((evaluations % 100) == 0) {
	        archive.printObjectivesToFile("FUN"+evaluations) ;
	        archive.printVariablesToFile("VAR"+evaluations) ;
	        archive.printObjectivesOfValidSolutionsToFile("FUNV"+evaluations) ;
	      }
	      */
	      
	      if ((indicators != null) && (requiredEvaluations == 0)) {
				double HV = indicators.getHypervolume(archive);
				if (HV >= (0.98 * indicators.getTrueParetoFrontHypervolume())) {
					requiredEvaluations = evaluations;
				} // if
			} // if
	    } while (evaluations < maxEvaluations);                                    
	        
	    //Return the  population of non-dominated solution
	    archive.printFeasibleFUN("FUN_PAES") ;
	    
	 // Return as output parameter the required evaluations
	    setOutputParameter("evaluations", requiredEvaluations);
	    
	    return archive;                
	  }  // execute  
	  
	  
	  
	  private boolean contains(SolutionSet population, Solution candidate) {
			Iterator<Solution> iterator = population.iterator();
			while (iterator.hasNext()) {
				Solution sol = iterator.next();
				if (equals(sol, candidate)) {
					return true;
				}
			}
			return false;
		}
	  
	  private static String solutionToString(Solution solution){
			String strSolution = "";
	    	Binary variable = ((Binary) solution.getDecisionVariables()[0]);
	    	boolean first = true;
			for (int i = 0; i < variable.getNumberOfBits(); i++) {
	            if (variable.bits_.get(i)) {
	            	if (first){
	            		strSolution += (i+1);
	            		first = false;
	            	}else{
	            		strSolution += "-" + (i+1); 
	            	}
	            }
			}
			strSolution += "";
			return strSolution;
		}
	  
		private boolean equals(Solution sol1, Solution sol2) {
			Binary s1 = (Binary)sol1.getDecisionVariables()[0];
			Binary s2 = (Binary)sol2.getDecisionVariables()[0];
			if (Arrays.equals(s1.bits_.toByteArray(), s2.bits_.toByteArray())){
				return true;
			}else{
				return false;
			}
		}
	  
} // PAES
