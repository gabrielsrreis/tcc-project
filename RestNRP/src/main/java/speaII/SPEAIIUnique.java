package speaII;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.*;
import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.encodings.variable.Binary;
import jmetal.metaheuristics.spea2.SPEA2;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.JMException;
import jmetal.util.Ranking;
import jmetal.util.Spea2Fitness;

public class SPEAIIUnique extends Algorithm{
	 /**
	   * Defines the number of tournaments for creating the mating pool
	   */
	  public static final int TOURNAMENTS_ROUNDS = 1;
	  List<String> listSolution = new ArrayList();
	  /**
	  * Constructor.
	  * Create a new SPEA2 instance
	  * @param problem Problem to solve
	  */
	  public SPEAIIUnique(Problem problem) {                
	    super(problem) ;
	  } // Spea2
	   
	  /**   
	  * Runs of the Spea2 algorithm.
	  * @return a <code>SolutionSet</code> that is a set of non dominated solutions
	  * as a result of the algorithm execution  
	   * @throws JMException 
	  */  
	  public SolutionSet execute() throws JMException, ClassNotFoundException {   
	    int populationSize, archiveSize, maxEvaluations, evaluations;
	    Operator crossoverOperator, mutationOperator, selectionOperator;
	    SolutionSet solutionSet, archive, offSpringSolutionSet;    
	    
	    
	    QualityIndicator indicators; // QualityIndicator object
		int requiredEvaluations; // Use in the example of use of the
		// indicators object (see below)
	    
	    //Read the params
	    populationSize = ((Integer)getInputParameter("populationSize")).intValue();
	    archiveSize    = ((Integer)getInputParameter("archiveSize")).intValue();
	    maxEvaluations = ((Integer)getInputParameter("maxEvaluations")).intValue();
	    indicators = (QualityIndicator) getInputParameter("indicators");
	        
	    //Read the operators
	    crossoverOperator = operators_.get("crossover");
	    mutationOperator  = operators_.get("mutation");
	    selectionOperator = operators_.get("selection");        
	        
	    //Initialize the variables
	    solutionSet  = new SolutionSet(populationSize);
	    archive     = new SolutionSet(archiveSize);
	    evaluations = 0;
	    requiredEvaluations = 0;
	    
	    
	    //-> Create the initial solutionSet
	    Solution newSolution;
	    for (int i = 0; i < populationSize; i++) {
	      newSolution = new Solution(problem_);
	      if (!contains(archive, newSolution) && !contains(solutionSet, newSolution)) {
			      problem_.evaluate(newSolution);            
			      problem_.evaluateConstraints(newSolution);
			      evaluations++;
			      solutionSet.add(newSolution);
	      }
	    }                        
	        
	    while (evaluations < maxEvaluations){               
	      SolutionSet union = ((SolutionSet)solutionSet).union(archive);
	      Spea2Fitness spea = new Spea2Fitness(union);
	      spea.fitnessAssign();
	      archive = spea.environmentalSelection(archiveSize);                       
	      // Create a new offspringPopulation
	      offSpringSolutionSet= new SolutionSet(populationSize);    
	      Solution  [] parents = new Solution[2];
	      while (offSpringSolutionSet.size() < populationSize){           
	        int j = 0;
	        do{
	          j++;                
	          parents[0] = (Solution)selectionOperator.execute(archive);
	          //TOURNAMENTS_ROUNDS indentificar o que � isso
	        } while (j < 1); // do-while                    
	        int k = 0;
	        do{
	          k++;                
	          parents[1] = (Solution)selectionOperator.execute(archive);
	          //TOURNAMENTS_ROUNDS indentificar o que � isso
	        } while (k < 1); // do-while
	            
	        //make the crossover 
	        Solution [] offSpring = (Solution [])crossoverOperator.execute(parents);            
	        mutationOperator.execute(offSpring[0]);
		        if ((!contains(archive, offSpring[0]) && !contains(archive, offSpring[1])) &&
		        		(!contains(solutionSet, offSpring[0]) && !contains(solutionSet, offSpring[1]))) {
		        problem_.evaluate(offSpring[0]);
		        problem_.evaluateConstraints(offSpring[0]);            
		        offSpringSolutionSet.add(offSpring[0]);
		        evaluations++;
		        }else {
//					System.err.println("Duplicate: " + offSpring[0] + " OR " + offSpring[1]);
				}
	      } // while
	      // End Create a offSpring solutionSet
	      solutionSet = offSpringSolutionSet;
	      
	      if ((indicators != null) && (requiredEvaluations == 0)) {
				double HV = indicators.getHypervolume(solutionSet);
				if (HV >= (0.98 * indicators.getTrueParetoFrontHypervolume())) {
					requiredEvaluations = evaluations;
				} // if
			} // if
	    } // while
	        
	 	// Return as output parameter the required evaluations
	    setOutputParameter("evaluations", requiredEvaluations);
	    
	    Ranking ranking = new Ranking(archive);
	    return ranking.getSubfront(0);
	  } // execute    
	  
	  
	  private boolean contains(SolutionSet population, Solution candidate) {
			Iterator<Solution> iterator = population.iterator();
			while (iterator.hasNext()) {
				Solution sol = iterator.next();
				if (equals(sol, candidate)) {
					return true;
				}
			}
			return false;
		}
	  
	  private static String solutionToString(Solution solution){
			String strSolution = "";
	    	Binary variable = ((Binary) solution.getDecisionVariables()[0]);
	    	boolean first = true;
			for (int i = 0; i < variable.getNumberOfBits(); i++) {
	            if (variable.bits_.get(i)) {
	            	if (first){
	            		strSolution += (i+1);
	            		first = false;
	            	}else{
	            		strSolution += "-" + (i+1); 
	            	}
	            }
			}
			strSolution += "";
			return strSolution;
		}
	  
		private boolean equals(Solution sol1, Solution sol2) {
			Binary s1 = (Binary)sol1.getDecisionVariables()[0];
			Binary s2 = (Binary)sol2.getDecisionVariables()[0];
			if (Arrays.equals(s1.bits_.toByteArray(), s2.bits_.toByteArray())){
				return true;
			}else{
				return false;
			}
		}
	  
} // SPEA2