package problem;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class Utils {
	public static double[][] readCostMatrix(String matrixFile,
			int numRequirements, int numObjectives) {
		double[][] matrix = new double[numRequirements][numObjectives];
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(
					matrixFile)));
			int row = 0;
			while (reader.ready()) {
				String line = reader.readLine();
				if (line.isEmpty() || line.matches("[a-zA-Z]+.*$")){ //;[a-zA-Z]+;[a-zA-Z]+;[a-zA-Z]+")) {
					continue;
				} else {
					// COST,VALUE
					String[] values = line.split(";");
					for (int col = 0; col < values.length; col++) {
						try {
							matrix[row][col] = Double.parseDouble(values[col]);
						} catch (NumberFormatException e) {

							matrix[row][col] = NumberFormat
									.getInstance(Locale.ITALIAN)
									.parse(values[col]).doubleValue();
						}
					}
					row++;
				}

			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return matrix;
	}

	public static Map<Integer, Set<Integer>> readDependencies(int numRequirements, String dependencyFile) {
		Map<Integer, Set<Integer>> dependencies = new HashMap<Integer, Set<Integer>> ();
//		Set<Integer> d5 = new HashSet<Integer> ();
//		d5.add(2);
//		d5.add(4);
//		dependencies.put(5, d5);
//		
//		Set<Integer> d2 = new HashSet<Integer> ();
//		d2.add(1);
//		dependencies.put(2, d2);
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(
					dependencyFile)));
			while (reader.ready()) {
				String line = reader.readLine();
				if (line.isEmpty() || line.matches("[a-zA-Z]+;[a-zA-Z]+")) {
					continue;
				} else {
					// REQ, DEP
					String[] values = line.split(";");
					int requirement = Integer.parseInt(values[0]);
					Set<Integer> deps = new HashSet<Integer>();
					for (String dep : values[1].split("-")) {
						deps.add(Integer.parseInt(dep));
					}
					dependencies.put(requirement, deps);
				}
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dependencies;
	}

	public static Map<Integer, Map<Integer, Set<Integer>>> readPriorities(int numRequirements, String prioritiesFile) {
		Map<Integer, Map<Integer, Set<Integer>>> priorities = new HashMap<Integer, Map<Integer, Set<Integer>>> ();
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(prioritiesFile)));
			while (reader.ready()) {
				String line = reader.readLine();
				if (line.isEmpty() || line.matches("[a-zA-Z]+;[a-zA-Z]+;[a-zA-Z]+")) {
					continue;
				} else {
					// CLIENT, REQ, DEP
					String[] values = line.split(";");
					int client = Integer.parseInt(values[0]);
					
					int req = Integer.parseInt(values[1]);
					
					Set<Integer> deps = new HashSet<Integer>();
					for (String dep : values[2].split("-")) {
						deps.add(Integer.parseInt(dep));
					}
					
					if (priorities.containsKey(client)){
						priorities.get(client).put(req, deps);
					}else{
						Map<Integer, Set<Integer>> priority = new HashMap<Integer, Set<Integer>> ();
						priority.put(req, deps);
						priorities.put(client, priority);
					}
				}
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return priorities;
	}
	
	public static void writeStringToFile (String content, String fileName){
		FileWriter out;
		try {
			out = new FileWriter(fileName);
			BufferedWriter writer = new BufferedWriter(out);
			writer.write(content);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
