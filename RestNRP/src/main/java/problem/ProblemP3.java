package problem;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.encodings.solutionType.BinarySolutionType;
import jmetal.encodings.variable.Binary;
import jmetal.util.JMException;

public class ProblemP3 extends Problem {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2310594913645720500L;
	private double[][] costMatrix;
	int row = 0;
	
	double[] MARGINES = {Double.MAX_VALUE, 0.0, Double.MAX_VALUE, Double.MAX_VALUE};

	Map<Integer, Set<Integer>> dependencies;
	Map<Integer, Map<Integer, Set<Integer>>> priorities;

	public ProblemP3(double[][] matrixFile, Map<Integer, Set<Integer>> dependencyFile, Map<Integer, Map<Integer, Set<Integer>>> prioritiesFile) {
		numberOfVariables_ = 1;
		numberOfObjectives_ = matrixFile[0].length;
		numberOfConstraints_ = 0;
		problemName_ = "ReqCVS";

		upperLimit_ = new double[numberOfVariables_];
		lowerLimit_ = new double[numberOfVariables_];
		
		solutionType_ = new BinarySolutionType(this);

		length_ = new int[numberOfVariables_];
		length_[0] = matrixFile.length;

		costMatrix = matrixFile;
		dependencies = dependencyFile;
		priorities = prioritiesFile;
	}

	@Override
	public void evaluate(Solution candidate) throws JMException {
		int objectives = candidate.getNumberOfObjectives();
		if (violatesConstraints(candidate)) {
			for (int i = 0; i < objectives; i++){
				candidate.setObjective(i, MARGINES[i]);
			}
//			candidate.setObjective(0, MAX_COST);
//			candidate.setObjective(1, MIN_VALUE);
//			candidate.setObjective(2, MAX_SIGMACOST);
//			candidate.setObjective(3, MAX_SIGMAVALUE);
		} else {
//			double cost = 0;
//			double value = 0;
//			double sigmacost = 0;
//			double sigmavalue = 0;
			double [] values = new double[objectives];
			for (int i = 0; i < objectives; i++){
				values[i] = 0.0;
			}
			Binary variable = ((Binary) candidate.getDecisionVariables()[0]);
			for (int i = 0; i < variable.getNumberOfBits(); i++) {
	            if (variable.bits_.get(i)) {
//	                // cost of ith requirement
//	                cost += costMatrix[i][0];
//
//	                // value of ith requirement
//	                value += costMatrix[i][1];
//
//	                // risk of ith requirement
//	                sigmacost += costMatrix[i][2];
//	                
//	             // risk of ith requirement
//	                sigmavalue += costMatrix[i][3];
	                
	                for (int j = 0; j < objectives; j++){
	                	values[j] += costMatrix[i][j];
	                }
	            }

	        }
			
			for (int i = 0; i < objectives; i++){
				if (i == 1)
					candidate.setObjective(i, -values[i]);
				else
					candidate.setObjective(i, values[i]);
			}
			
//			candidate.setObjective(0, cost);
//			candidate.setObjective(1, -value);
//			candidate.setObjective(2, sigmacost);
//			candidate.setObjective(3, sigmavalue);
		}

	}

	private boolean violatesConstraints(Solution solution) throws JMException {
		boolean violates = false;

//		// check for maximum cost
//		if (solution.getObjective(0) > MAX_COST) {
//			return true;
//		}
//
//		// check for minimum value
//		if (solution.getObjective(1) * -1 < MIN_VALUE) {
//			return true;
//
//		}
//		// check for maximum Sigmacost
//		if (solution.getObjective(2) > MAX_SIGMACOST) {
//			return true;
//		}
//		// check for maximum SigmaValue
//		if (solution.getObjective(3) > MAX_SIGMAVALUE) {
//			return true;
//		}
		// check for dependencies
		List<Integer> sol = new ArrayList<Integer>();
		Binary variable = ((Binary) solution.getDecisionVariables()[0]);
		for (int i = 0; i < variable.getNumberOfBits(); i++) {
            if (variable.bits_.get(i)) {
            	sol.add(i+1);
            }
		}

		for (Integer i : sol) {
			Set<Integer> deps = dependencies.get(i);
			if (deps == null) {
				continue;
			}
			if (!sol.containsAll(deps)) {
				violates = true;
				break;
			}
		}

		if (violates) {
			return violates;
		}

		// check for client priority
		for (Entry<Integer, Map<Integer, Set<Integer>>> entry : priorities
				.entrySet()) {
			for (Integer i : sol) {
				Set<Integer> deps = entry.getValue().get(i);
				if (deps == null) {
					continue;
				}
				if (!sol.containsAll(deps)) {
					violates = true;
					break;
				}
			}
			if (violates) {
				break;
			}
		}

		return violates;
	}

}
