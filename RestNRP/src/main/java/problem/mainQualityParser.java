package problem;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.QualityLog;

public class mainQualityParser {

	public static void main(String[] args) {
		if(args.length < 2) {
			System.out.println(String.format("Quantidade de parametros incorretos.\n %s\n%s", args[0], args[1]));
			return;
		}
		
		// Minera os dados no arquivo de output e os salva em um arquivo CSV
		writeCSVFile(getLogs(args[0]), args[1]);
	}
	
	public static void writeCSVFile(List<QualityLog> logs, String savePath) {
		String content = "sep=,\nTotalExecutionTime, Hypervolume, GD, IGD, Spread, Epsilon, Speed\n";
		
		for(QualityLog log : logs) {
			content += String.format(Locale.US, "%d,%f,%f,%f,%f,%f,%d\n",
					log.getTotalExecutionTime(),
					log.getHypervolume(),
					log.getGd(),
					log.getIgd(),
					log.getSpread(),
					log.getEpsilon(),
					log.getSpeed()					
			);
		}
		
		writeStringToFile(content, String.format("%s\\%s", savePath, "output_statistics.csv"));
	}
	
	public static void writeStringToFile(String content, String fileName) {
		FileWriter out;
		try {
			out = new FileWriter(fileName);
			BufferedWriter writer = new BufferedWriter(out);
			writer.write(content);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static List<QualityLog> getLogs(String filePath) {
		List<QualityLog> logs = new ArrayList();
		// Define os Patterns
		Pattern patternTotalExecutionTime = Pattern.compile("Total execution time: [0-9]+ms");
		Pattern patternHypervolume = Pattern.compile("Hypervolume: [0-9]+.[0-9]+");
		Pattern patternGD = Pattern.compile("GD[ ]*: [0-9]+.[0-9]+");
		Pattern patternIGD = Pattern.compile("IGD[ ]*: [0-9]+.[0-9]+");
		Pattern patternSpread = Pattern.compile("Spread[ ]*: [0-9]+.[0-9]+");
		Pattern patternEpsilon = Pattern.compile("Epsilon[ ]*: [0-9]+.[0-9]+");
		Pattern patternSpeed = Pattern.compile("Speed[ ]*: [0-9]+.[0-9]+");

		// Define o buffer do arquivo
		String fileContent = "";

		// Alimenta o buffer do arquivo lendo o arquivo passado por parametro de execu��o
		try {
			String line;
			BufferedReader reader = new BufferedReader(new FileReader(new File(filePath)));
			// Compacta buffer em uma unica String
			while ((line = reader.readLine()) != null) {
				fileContent += line;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Inicializa os matcher para a busca dos valores requeridos
		Matcher matcherTotalExecutionTime = patternTotalExecutionTime.matcher(fileContent);
		Matcher matcherHypervolume = patternHypervolume.matcher(fileContent);
		Matcher matcherGD = patternGD.matcher(fileContent);
		Matcher matcherIGD = patternIGD.matcher(fileContent);
		Matcher matcherSpread = patternSpread.matcher(fileContent);
		Matcher matcherEpsilon = patternEpsilon.matcher(fileContent);
		Matcher matcherSpeed = patternSpeed.matcher(fileContent);

		try {
			// Extrai os valores e os passa para a classe de QualityLog para uso posterior
			while (matcherTotalExecutionTime.find()) {
				matcherHypervolume.find();
				matcherGD.find();
				matcherIGD.find();
				matcherSpread.find();
				matcherEpsilon.find();
				matcherSpeed.find();
				
				QualityLog log = new QualityLog(
						getInteger(matcherTotalExecutionTime.group()),
						getDouble(matcherHypervolume.group()),
						getDouble(matcherGD.group()),
						getDouble(matcherIGD.group()),
						getDouble(matcherSpread.group()),
						getDouble(matcherEpsilon.group()),
						getInteger(matcherSpeed.group())
				);
				
				logs.add(log);
			}
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		
		return logs;
	}
	
	public static double getDouble(String str) {
		Pattern patternDouble = Pattern.compile("[0-9]+.[0-9]+");
		Matcher m = patternDouble.matcher(str);
		
		if(m.find()) {
			return Double.parseDouble(m.group());
		} else {
			return 0;
		}
	}
	
	public static int getInteger(String str) {
		Pattern patternDouble = Pattern.compile("[0-9]+");
		Matcher m = patternDouble.matcher(str);
		
		if(m.find()) {
			return Integer.parseInt(m.group());
		} else {
			return 0;
		}
	}
}
