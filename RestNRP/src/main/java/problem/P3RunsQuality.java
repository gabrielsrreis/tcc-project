package problem;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.encodings.variable.Binary;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import nsgaII.NSGAIIUnique;

/**
 * Class to configure and execute the NSGA-II algorithm.
 * 
 * Besides the classic NSGA-II, a steady-state version (ssNSGAII) is also
 * included (See: J.J. Durillo, A.J. Nebro, F. Luna and E. Alba "On the Effect
 * of the Steady-State Selection Scheme in Multi-Objective Genetic Algorithms"
 * 5th International Conference, EMO 2009, pp: 183-197. April 2009)
 */

public class P3RunsQuality {
	public static Logger logger_; // Logger object
	public static FileHandler fileHandler_; // FileHandler object

	/**
	 * @param args
	 *            Command line arguments.
	 * @throws JMException
	 * @throws IOException
	 * @throws SecurityException
	 *             Usage: three options -
	 *             jmetal.metaheuristics.nsgaII.NSGAII_main -
	 *             jmetal.metaheuristics.nsgaII.NSGAII_main problemName -
	 *             jmetal.metaheuristics.nsgaII.NSGAII_main problemName
	 *             paretoFrontFile
	 */
	public static void main(String[] args) throws JMException,
			SecurityException, IOException, ClassNotFoundException {
		
		if (args.length < 5) {
			System.out.println("Enter the number of requirements, number of objectives, the cost matrix file, the dependency file, and the client priorities file");
			System.exit(1);
		}
		int numberOfRequirements = Integer.parseInt(args[0]);
		int numberOfObjectives = Integer.parseInt(args[1]);
//		double maxCost = Double.parseDouble(args[2]);
//		double minValue = Double.parseDouble(args[3]);
//		double maxSigmaCost = Double.parseDouble(args[4]);
//		double maxSigmaValue = Double.parseDouble(args[5]);
		String dependencyFile = args[2];
		String prioritiesFile = args[3];
		String matrixFile = args[4];
		
		// Logger object and file to store log messages
		logger_ = Configuration.logger_;
		fileHandler_ = new FileHandler("NSGAII_main.log");
		logger_.addHandler(fileHandler_);
		
		int NUM_RUNS = 10;
		for (int run = 1; run <= NUM_RUNS; run++){
		   
			String funFile = "FUN_" + run;
		    String varFile = "VAR_" + run;			
		    String solutionsFile = "solutions_" + run + ".csv";
		    String dissimilarityFile = "DissimilarityMatrix_" + run + ".txt";
		    
		   Problem problem; // The problem to solve
		   Algorithm algorithm; // The algorithm to use
		   Operator crossover; // Crossover operator
		   Operator mutation; // Mutation operator
		   Operator selection; // Selection operator

		HashMap parameters; // Operator parameters

		QualityIndicator indicators; // Object to get quality indicators

	

		problem = null;
		algorithm = new NSGAIIUnique(problem);
		// algorithm = new ssNSGAII(problem);

		// Algorithm parameters
		algorithm.setInputParameter("populationSize", 180);
		algorithm.setInputParameter("maxEvaluations", 1000000);

		// Mutation and Crossover for Real codification
		parameters = new HashMap();
		parameters.put("probability", 0.8);
		parameters.put("distributionIndex", 20.0);
		crossover = CrossoverFactory.getCrossoverOperator("SinglePointCrossover", parameters);

		parameters = new HashMap();
		parameters.put("probability", 1.0 / numberOfRequirements);
		parameters.put("distributionIndex", 20.0);
		mutation = MutationFactory.getMutationOperator("BitFlipMutation", parameters);

		// Selection Operator
		parameters = null;
		selection = SelectionFactory.getSelectionOperator("BinaryTournament2",
				parameters);

		// Add the operators to the algorithm
		algorithm.addOperator("crossover", crossover);
		algorithm.addOperator("mutation", mutation);
		algorithm.addOperator("selection", selection);

		// Add the indicator object to the algorithm
		indicators = new QualityIndicator(problem, "FUN") ;
		algorithm.setInputParameter("indicators", indicators);

		// Execute the Algorithm
		long initTime = System.currentTimeMillis();
		SolutionSet population = algorithm.execute();
		long estimatedTime = System.currentTimeMillis() - initTime;

		// Result messages
		System.out.print("\n");
		logger_.info("Total execution time: " + estimatedTime + "ms");
		logger_.info("Variables values have been writen to file VAR");
		population.printVariablesToFile(varFile);
		logger_.info("Objectives values have been writen to file FUN");
		population.printObjectivesToFile(funFile);

		printSolutionsToFile(population, solutionsFile);
		
		generateDissimilarityMatrix(population, dissimilarityFile);
		
		if (indicators != null) {
			logger_.info("Quality indicators");
			logger_.info("Hypervolume: "
					+ indicators.getHypervolume(population));
			logger_.info("GD         : " + indicators.getGD(population));
			logger_.info("IGD        : " + indicators.getIGD(population));
			logger_.info("Spread     : " + indicators.getSpread(population));
			logger_.info("Epsilon    : " + indicators.getEpsilon(population));

			int evaluations = ((Integer) algorithm
					.getOutputParameter("evaluations")).intValue();
			logger_.info("Speed      : " + evaluations + " evaluations");
		}
		}
	} // main
	
	public static void printSolutionsToFile(SolutionSet population, String path){
	    try {
	      /* Open the file */
	      FileOutputStream fos   = new FileOutputStream(path)     ;
	      OutputStreamWriter osw = new OutputStreamWriter(fos)    ;
	      BufferedWriter bw      = new BufferedWriter(osw)        ;

	      String header = "Solution,"; // Cost, Values, SigmaCost, SigmaValue,";
	      int numObj = population.get(0).getNumberOfObjectives();
	      for (int i = 1; i <= numObj; i++){
	    	  header += "Obj" + i + ",";
	      }
	      bw.write(header);
	      bw.newLine();
	      Iterator<Solution> iterator = population.iterator();
	      while (iterator.hasNext()) {
	    	Solution solution = iterator.next();
	        
	    	String strSolution = solutionToString(solution);
			bw.write("[" + strSolution + "],");
	        
	        for (int j = 0; j < solution.getNumberOfObjectives(); j++)
	            bw.write(solution.getObjective(j) + ",");
	        
	        bw.newLine();
	      }

	      /* Close the file */
	      bw.close();
	    }catch (IOException e) {
	      Configuration.logger_.severe("Error acceding to the file");
	      e.printStackTrace();
	    }
	  } // printObjectivesToFile
	
	private static String solutionToString(Solution solution){
		String strSolution = "";
    	Binary variable = ((Binary) solution.getDecisionVariables()[0]);
    	boolean first = true;
		for (int i = 0; i < variable.getNumberOfBits(); i++) {
            if (variable.bits_.get(i)) {
            	if (first){
            		strSolution += (i+1);
            		first = false;
            	}else{
            		strSolution += "-" + (i+1); 
            	}
            }
		}
		strSolution += "";
		return strSolution;
	}
	
	private static void generateDissimilarityMatrix (SolutionSet population, String dissimilarityMatrixFileName){
		// generate distance matrix
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < population.size(); i++){
			for(int j = 0; j < i; j++){
				double d = computeDissimilarity(population.get(i), population.get(j));
				String line = "\"" + solutionToString(population.get(i)) + "\" \"" + solutionToString(population.get(j)) + "\" " + d + "\n";
				buffer.append(line);
				//System.out.println (line);
			}
		}
		writeStringToFile(buffer.toString(), dissimilarityMatrixFileName);
	}
	
	
	private static double computeDissimilarity(Solution solution1, Solution solution2) {
		double dist = 0;
		for (int i = 0; i < solution1.getNumberOfObjectives(); i++){
			dist += Math.sqrt( Math.pow(solution2.getObjective(i)-solution1.getObjective(i), 2));
		}
		
		return dist;
	}
	
	public static void writeStringToFile (String content, String fileName){
		FileWriter out;
		try {
			out = new FileWriter(fileName);
			BufferedWriter writer = new BufferedWriter(out);
			writer.write(content);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
} // NSGAII_main
