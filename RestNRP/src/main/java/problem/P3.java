package problem;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import com.nrp.ws.rest.vo.InputVO;
import com.nrp.ws.rest.vo.OutputVO;
import com.nrp.ws.rest.vo.SolucaoVO;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.encodings.variable.Binary;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import mocell.MOCellUnique;
import nsgaII.NSGAIIUnique;
import paes.PAESUnique;
import speaII.SPEAIIUnique;

/**
 * Class to configure and execute the NSGA-II algorithm.
 * 
 * Besides the classic NSGA-II, a steady-state version (ssNSGAII) is also
 * included (See: J.J. Durillo, A.J. Nebro, F. Luna and E. Alba "On the Effect
 * of the Steady-State Selection Scheme in Multi-Objective Genetic Algorithms"
 * 5th International Conference, EMO 2009, pp: 183-197. April 2009)
 */

public class P3 {
	public static Logger logger_; // Logger object
	public static FileHandler fileHandler_; // FileHandler object

	public static OutputVO exec(double[][] matrixFile, Map<Integer, Set<Integer>> dependencyFile,
			Map<Integer, Map<Integer, Set<Integer>>> prioritiesFile, InputVO input) {
		try {
			return execute(matrixFile, dependencyFile, prioritiesFile, input);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * @param args Command line arguments.
	 * @throws JMException
	 * @throws IOException
	 * @throws SecurityException Usage: three options -
	 *                           jmetal.metaheuristics.nsgaII.NSGAII_main -
	 *                           jmetal.metaheuristics.nsgaII.NSGAII_main
	 *                           problemName -
	 *                           jmetal.metaheuristics.nsgaII.NSGAII_main
	 *                           problemName paretoFrontFile
	 */
	public static OutputVO execute(double[][] matrixFile, Map<Integer, Set<Integer>> dependencyFile,
			Map<Integer, Map<Integer, Set<Integer>>> prioritiesFile, InputVO input)
			throws JMException, SecurityException, IOException, ClassNotFoundException {

		Problem problem; // The problem to solve
		Algorithm algorithm = null; // The algorithm to use
		Operator crossover; // Crossover operator
		Operator mutation; // Mutation operator
		Operator selection; // Selection operator

		HashMap parameters; // Operator parameters

		logger_ = Configuration.logger_;
		fileHandler_ = new FileHandler("NSGAII_main.log");
		logger_.addHandler(fileHandler_);

		problem = new ProblemP3(matrixFile, dependencyFile, prioritiesFile);
		if(input.getAlgoritm().equals("N")) {
			algorithm = new NSGAIIUnique(problem);
		} else if(input.getAlgoritm().equals("M")) {
			algorithm = new MOCellUnique(problem);
		} if(input.getAlgoritm().equals("P")) {
			algorithm = new PAESUnique(problem);
		} if(input.getAlgoritm().equals("S")) {
			algorithm = new SPEAIIUnique(problem);
		}
		// algorithm = new ssNSGAII(problem);

		// Algorithm parameters
		algorithm.setInputParameter("populationSize", input.getConfiguration().getPopulationSize());
		algorithm.setInputParameter("archiveSize",input.getConfiguration().getPopulationSize());
		algorithm.setInputParameter("maxEvaluations", input.getConfiguration().getMaxEvaluations());
		algorithm.setInputParameter("biSections",5);

		// Mutation and Crossover for Real codification
		parameters = new HashMap();
		parameters.put("probability", input.getConfiguration().getProbabilityMut());
		parameters.put("distributionIndex", input.getConfiguration().getDistributionIndex());
		algorithm.setInputParameter("feedBack",20);
		crossover = CrossoverFactory.getCrossoverOperator(input.getConfiguration().getCrossover(), parameters);

		parameters = new HashMap();
		parameters.put("probability", input.getConfiguration().getProbabilityMut() / matrixFile.length);
		parameters.put("distributionIndex", input.getConfiguration().getDistributionIndex());
		mutation = MutationFactory.getMutationOperator(input.getConfiguration().getMutation(), parameters);

		// Selection Operator
		parameters = null;
		selection = SelectionFactory.getSelectionOperator(input.getConfiguration().getSelection(), parameters);

		// Add the operators to the algorithm
		algorithm.addOperator("crossover", crossover);
		algorithm.addOperator("mutation", mutation);
		algorithm.addOperator("selection", selection);


		// Execute the Algorithm
		long initTime = System.currentTimeMillis();
		SolutionSet population = algorithm.execute();
		long estimatedTime = System.currentTimeMillis() - initTime;

		// Result messages
		logger_.info("Total execution time: " + estimatedTime + "ms");
		logger_.info("Variables values have been writen to file VAR");
		population.printVariablesToFile("VAR");
		logger_.info("Objectives values have been writen to file FUN");
		population.printObjectivesToFile("FUN");

		OutputVO list = printSolutionsToFile(population, "solutions.csv");
		
		if(input.getAlgoritm().equals("N")) {
			list.setAlgoritmo("NSGA-II");
		} else if(input.getAlgoritm().equals("M")) {
			list.setAlgoritmo("MOCell");
		} if(input.getAlgoritm().equals("P")) {
			list.setAlgoritmo("PAES");
		} if(input.getAlgoritm().equals("S")) {
			list.setAlgoritmo("SPEA-II");
		}
		list.setExecutionTime(estimatedTime);
		list.setNumSolucoes(list.getOut().size());

		return list;
	} // main

	public static OutputVO printSolutionsToFile(SolutionSet population, String path) {
		List<SolucaoVO> list = new ArrayList<SolucaoVO>();
		OutputVO out = new OutputVO();
		try {
			/* Open the file */
			FileOutputStream fos = new FileOutputStream(path);
			OutputStreamWriter osw = new OutputStreamWriter(fos);
			BufferedWriter bw = new BufferedWriter(osw);
			String header = "Solution,"; // Cost, Values, SigmaCost, SigmaValue,";
			int numObj = population.get(0).getNumberOfObjectives();
			for (int i = 1; i <= numObj; i++) {
				header += "Obj" + i + ",";
			}
			bw.write(header);
			bw.newLine();
			Iterator<Solution> iterator = population.iterator();
			while (iterator.hasNext()) {
				Solution solution = iterator.next();
				SolucaoVO solucao = new SolucaoVO();
				solucao.setConjunto(solutionToString(solution));
				String strSolution = solutionToString(solution);
				bw.write("[" + strSolution + "],");
				List<Double> objtivos = new ArrayList<Double>();
				for (int j = 0; j < solution.getNumberOfObjectives(); j++) {
					double objective = solution.getObjective(j);
					if (objective < 0) {
						objective = objective * -1;
					}
					objtivos.add(j, objective);
					bw.write(objective + ",");
				}
				bw.newLine();
				solucao.setObjetivos(objtivos);
				list.add(solucao);
			}

			/* Close the file */
			bw.close();
		} catch (IOException e) {
			Configuration.logger_.severe("Error acceding to the file");
			e.printStackTrace();
		}
		out.setOut(list);
		return out;
	} // printObjectivesToFile

	private static String solutionToString(Solution solution) {
		String strSolution = "";
		Binary variable = ((Binary) solution.getDecisionVariables()[0]);
		boolean first = true;
		for (int i = 0; i < variable.getNumberOfBits(); i++) {
			if (variable.bits_.get(i)) {
				if (first) {
					strSolution += (i + 1);
					first = false;
				} else {
					strSolution += "-" + (i + 1);
				}
			}
		}
		strSolution += "";
		return strSolution;
	}

	private static void generateDissimilarityMatrix(SolutionSet population, String dissimilarityMatrixFileName) {
		// generate distance matrix
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < population.size(); i++) {
			for (int j = 0; j < i; j++) {
				double d = computeDissimilarity(population.get(i), population.get(j));
				String line = "\"" + solutionToString(population.get(i)) + "\" \"" + solutionToString(population.get(j))
						+ "\" " + d + "\n";
				buffer.append(line);
				// System.out.println (line);
			}
		}
		writeStringToFile(buffer.toString(), dissimilarityMatrixFileName);
	}

	private static double computeDissimilarity(Solution solution1, Solution solution2) {
		double dist = 0;
		for (int i = 0; i < solution1.getNumberOfObjectives(); i++) {
			dist += Math.sqrt(Math.pow(solution2.getObjective(i) - solution1.getObjective(i), 2));
		}

		return dist;
	}

	public static void writeStringToFile(String content, String fileName) {
		FileWriter out;
		try {
			out = new FileWriter(fileName);
			BufferedWriter writer = new BufferedWriter(out);
			writer.write(content);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

} // NSGAII_main
