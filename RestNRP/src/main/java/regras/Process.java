package regras;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.nrp.ws.rest.vo.InputVO;
import com.nrp.ws.rest.vo.OutputVO;

import problem.P3;

public class Process {

	public OutputVO processar(InputVO input) {
		double[][] costos = extrairCusto(input);
		Map<Integer, Set<Integer>> dependencias = extrairDependencias(input);
		Map<Integer, Map<Integer, Set<Integer>>> prioridades = extrairPrioridade(input);
		return P3.exec(costos, dependencias, prioridades, input);
	}

	public double[][] extrairCusto(InputVO input) {
		double[][] matrix;
		int row = 0;
		String[] lines = input.getCost().split("\r\n");
		if (lines.length < 2) {
			lines = input.getDependencia().split("\n");
		}
		String[] cols = lines[0].split(";");
		if (cols.length < 2) {
			cols = lines[0].split(",");
		}
		matrix = new double[lines.length - 1][cols.length];
		if (lines.length >= 2) {
			for (String line : lines) {
				if (line.isEmpty() || line.matches("[a-zA-Z]+.*$")) { // ;[a-zA-Z]+;[a-zA-Z]+;[a-zA-Z]+")) {
					continue;
				} else {
					// COST,VALUE
					String[] values = line.split(";");
					if (values.length < 2) {
						values = line.split(",");
					}
					for (int col = 0; col < values.length; col++) {
						try {
							matrix[row][col] = Double.parseDouble(values[col]);
						} catch (NumberFormatException e) {
							try {
								matrix[row][col] = NumberFormat.getInstance(Locale.ITALIAN).parse(values[col])
										.doubleValue();
							} catch (ParseException e1) {

							}
						}
					}
					row++;
				}
			}
		}
		return matrix;
	}

	public Map<Integer, Set<Integer>> extrairDependencias(InputVO input) {
		Map<Integer, Set<Integer>> dependencies = new HashMap<Integer, Set<Integer>>();
		if (input.getDependencia() != null) {
			String[] lines = input.getDependencia().split("\r\n");
			if (lines.length < 2) {
				lines = input.getDependencia().split("\n");
			}
			if (lines.length >= 2) {
				for (String line : lines) {
					if (line.isEmpty() || line.matches("[a-zA-Z]+;[a-zA-Z]+;[a-zA-Z]+")) {
						continue;
					} else {
						// REQ, DEP
						String[] values = line.split(";");
						if (values.length < 2) {
							values = line.split(",");
						}
						int requirement = Integer.parseInt(values[0]);
						Set<Integer> deps = new HashSet<Integer>();
						for (String dep : values[1].split("-")) {
							deps.add(Integer.parseInt(dep));
						}
						dependencies.put(requirement, deps);
					}
				}
			}
		}
		return dependencies;
	}

	public Map<Integer, Map<Integer, Set<Integer>>> extrairPrioridade(InputVO input) {
		Map<Integer, Map<Integer, Set<Integer>>> priorities = new HashMap<Integer, Map<Integer, Set<Integer>>>();
		if (input.getPrioridade() != null) {
			String[] lines = input.getPrioridade().split("\r\n");
			if (lines.length < 2) {
				lines = input.getPrioridade().split("\n");
			}
			if (lines.length >= 2) {
				for (String line : lines) {
					if (line.isEmpty() || line.matches("[a-zA-Z]+;[a-zA-Z]+;[a-zA-Z]+")) {
						continue;
					} else {
						// CLIENT, REQ, DEP
						String[] values = line.split(";");
						if (values.length < 2) {
							values = line.split(",");
						}
						int client = Integer.parseInt(values[0]);

						int req = Integer.parseInt(values[1]);

						Set<Integer> deps = new HashSet<Integer>();
						for (String dep : values[2].split("-")) {
							deps.add(Integer.parseInt(dep));
						}

						if (priorities.containsKey(client)) {
							priorities.get(client).put(req, deps);
						} else {
							Map<Integer, Set<Integer>> priority = new HashMap<Integer, Set<Integer>>();
							priority.put(req, deps);
							priorities.put(client, priority);
						}
					}
				}
			}
		}
		return priorities;
	}

}
