package model;

public class QualityLog {
	private int totalExecutionTime;
	private double hypervolume;
	private double gd;
	private double igd;
	private double spread;
	private double epsilon;
	private int speed;
	
	public QualityLog(int totalExecutionTime, double hypervolume, double gd, double igd, double spread, double epsilon, int speed) {
		this.totalExecutionTime = totalExecutionTime;
		this.hypervolume = hypervolume;
		this.gd = gd;
		this.igd = igd;
		this.spread = spread;
		this.epsilon = epsilon;
		this.speed = speed;
	}

	public int getTotalExecutionTime() {
		return totalExecutionTime;
	}

	public void setTotalExecutionTime(int totalExecutionTime) {
		this.totalExecutionTime = totalExecutionTime;
	}

	public double getHypervolume() {
		return hypervolume;
	}

	public void setHypervolume(double hypervolume) {
		this.hypervolume = hypervolume;
	}

	public double getGd() {
		return gd;
	}

	public void setGd(double gd) {
		this.gd = gd;
	}

	public double getIgd() {
		return igd;
	}

	public void setIgd(double igd) {
		this.igd = igd;
	}

	public double getSpread() {
		return spread;
	}

	public void setSpread(double spread) {
		this.spread = spread;
	}

	public double getEpsilon() {
		return epsilon;
	}

	public void setEpsilon(double epsilon) {
		this.epsilon = epsilon;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}
}
